import 'package:flutter/material.dart';

class Category {
  //Class Constructor
  Category({
    required this.id,
    required this.title,
    this.color = Colors.red,
  });

  //Class Fields
  final String id;
  final String title;
  final Color color;

  //Class Methods
}
