import 'package:flutter/material.dart';
import '../dummy_data.dart';
import 'package:mealsapp/widgets/category_item.dart';



class CategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('DeliMeals')),
      body: GridView(
        padding: const EdgeInsets.all(25),
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
          children: DUMMY_CATEGORIES.map((catData) => CategoryItem(id: catData.id,title: catData.title, color: catData.color)).toList()
      )
    );
  }
}
