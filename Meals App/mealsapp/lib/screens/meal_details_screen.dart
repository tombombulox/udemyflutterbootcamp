import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../dummy_data.dart';

class MealDetailsScreen extends StatelessWidget {
  static const routeName = "/meal-details";

  @override
  Widget build(
    BuildContext context,
  ) {
    Widget BuildSectionTitle(BuildContext context, String title) {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: Text(title, style: Theme.of(context).textTheme.titleLarge),
      );
    }

    Widget BuildContainer(Widget child) {
      return Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(10)),
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          height: 150,
          width: 300,
          child: child);
    }

    //ModalRoute Arguments
    var mealId = ModalRoute.of(context)?.settings.arguments ?? -1;

    // This meal's Data Object.
    var mealObject = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);
    return Scaffold(
      appBar: AppBar(title: Text("${mealObject.title}")),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // RECIPE IMAGE
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                mealObject.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            // INGREDIENTS TEXT LABEL
            BuildSectionTitle(context, "Ingredients"),
            // INGREDIENTS LIST
            BuildContainer(ListView.builder(
              itemCount: mealObject.ingredients.length,
              itemBuilder: (context, index) {
                return Card(
                  child: Text(mealObject.ingredients[index]),
                );
              },
            )),
            BuildSectionTitle(context, "Instructions"),
            BuildContainer(ListView.builder(
              itemCount: mealObject.steps.length,
              itemBuilder: (context, index) => ListTile(
                  leading: CircleAvatar(
                    child: Text("${index}"),
                  ),
                  title: Text(mealObject.steps[index])),
            ))
          ],
        ),
      ),
    );
  }
}
